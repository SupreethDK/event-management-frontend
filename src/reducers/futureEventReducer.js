import { GET_FUTURE_EVENTS } from '../actions/actionTypes';

const initialState = {
        futureEvents : [],
}

const futureEventReducer = (state = initialState, action) => {
    switch(action.type) {

        case GET_FUTURE_EVENTS:
            return {
                ...state,
                futureEvents : action.payload
            }
        
            default: return {...state}
    }
}

export default futureEventReducer;
