import { combineReducers } from "redux";
import pastEventReducer from "./pastEventReducer";
import futureEventReducer from "./futureEventReducer";

export default combineReducers({
    pastEventReducer,
    futureEventReducer
})