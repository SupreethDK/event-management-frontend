import { GET_PAST_EVENTS } from '../actions/actionTypes';

const initialState = {
    pastEvents :  [],
}

const pastEventReducer = (state = initialState, action) => {

    switch(action.type) {
        
        case GET_PAST_EVENTS:
            return {
                ...state,
                pastEvents :  action.payload
            } 
        
        default: return {...state}
    }
}

export default pastEventReducer;
