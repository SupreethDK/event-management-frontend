import React from "react";
import { Button, ButtonToolbar } from 'react-bootstrap';
import Booking from './Booking';
import CardFuture from './CardFuture';
import { connect } from 'react-redux';
import { getFutureEvents } from '../actions/actionCreators';

class FutureEvent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            showModal: false
        }
        this.modalclose = this.modalclose.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    componentDidMount() {
        fetch('https://supreeth-evm.herokuapp.com/events/future')
            .then(res => res.json())
            .then(res => this.props.dispatch(getFutureEvents(res)))
            .catch(err => console.error(err))
    }

    modalclose() {
        this.setState({ showModal: false })
    }

    handleDelete(e) {

        let id = e.target.id;

        try {
            fetch(`https://supreeth-evm.herokuapp.com/events/${id}`, {
                method: "DELETE",
                headers: { "Content-Type": "application/json; charset=UTF-8" },
            })
                .then(response => response.json())
                .then(res => console.log(res))
                .catch(err => console.error(err))

            let remainingEvents = this.props.futureEvents.filter(event => event.event_id !== id)
            this.props.dispatch(getFutureEvents(remainingEvents))
        } catch (err) {
            console.error(err.message);
        }

    }

    render() {
        return (
            <div>
                <p className="h4 text-center p-3 mb-2 bg-title-color text-white">Upcoming Events</p>
                <div className="text-success d-flex flex-column align-items-center">
                    {this.props.futureEvents.map((event) => <CardFuture event={event} key={event.event_id} onDelete={this.handleDelete} />)}
                </div>
                <div className="d-flex justify-content-center p-2 m-3 ">
                    <ButtonToolbar>
                        <Button varient='primary' onClick={() => this.setState({ showModal: true })}>
                            Book Ticket
                    </Button>
                        <Booking show={this.state.showModal} onHide={this.modalclose} />
                    </ButtonToolbar>
                </div>
            </div>
        )
    }

}

function mapStateToProps(state) {
    return {
        futureEvents: state.futureEventReducer.futureEvents
    }
}

export default connect(mapStateToProps)(FutureEvent);
