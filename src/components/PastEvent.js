import React from "react";
import History from './History';
import { connect } from 'react-redux';
import { getPastEvents } from '../actions/actionCreators';

class PastEvent extends React.Component {

    componentDidMount() {
        fetch('https://supreeth-evm.herokuapp.com/events/past')
            .then(res => res.json())
            .then(res => this.props.dispatch(getPastEvents(res)))
            .catch(err => console.error(err))
    }

    render() {
        return (
            <div>
                <p className="h4 text-center p-3 mb-2 bg-title-color text-white">Past Events Managed</p>
                <div className="text-success d-flex flex-column align-items-center">{this.props.pastEvents.map((event) => <History key={event.event_id} event={event} />)}</div>
            </div>
        )
    }

}

function mapStateToProps(state) {
    return {
        pastEvents: state.pastEventReducer.pastEvents
    }
}

export default connect(mapStateToProps)(PastEvent);
