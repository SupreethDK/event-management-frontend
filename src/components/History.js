import React from "react";


class History extends React.Component {

    render() {
        return (
            <div className=" d-flex justify-content-center">
                <div className="row m-2 p-2" key={this.props.event.event_id}>
                    <div className="col-md-12">
                        <div className="card text-center">
                            <div className="card-body">
                                <h5 className="card-title">{this.props.event.event_name}</h5>
                                <p className="card-text">{this.props.event.event_date}</p>
                                <p className="card-text">{this.props.event.location}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}



export default History;