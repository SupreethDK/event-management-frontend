import React from "react";

function Footer() {
  const year = new Date().getFullYear();
  return (
    <footer className="text-center mt-3 p-3">
      <p>Copyright ⓒ {year}</p>
    </footer>
  );
}

export default Footer;
