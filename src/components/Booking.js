import React from "react";
import { Modal, Button, Row, Col, Form } from 'react-bootstrap';

class Booking extends React.Component {

    constructor(props) {
        super();
        this.state = {
            eventName: '',
            participantNamer: '',
            email: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }


    handleSubmit(e) {
        e.preventDefault();
        fetch('https://supreeth-evm.herokuapp.com/events/booking', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                eventName: this.state.eventName,
                participantName: this.state.participantName,
                email: this.state.email
            })
        })
            .then(res => res.json())
            .then(() => {
                alert('Success');
                window.location.reload()
            })
            .catch((error) => { alert('Try again') })
    }

    render() {
        return (
            <Modal
                {...this.props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Book Ticket
                </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className='book-container'>
                        <Row>
                            <Col>
                                <Form className="d-flex flex-column" onSubmit={this.handleSubmit}>
                                    <Form.Group controlId="eventName">
                                        <Form.Label>Enter event choice</Form.Label>
                                        <Form.Control
                                            type="text"
                                            name="eventName"
                                            placeholder="Event"
                                            onChange={(this.handleChange)}
                                            required />
                                    </Form.Group>
                                    <Form.Group controlId="participantName">
                                        <Form.Label>Enter Name</Form.Label>
                                        <Form.Control
                                            type="text"
                                            name="participantName"
                                            placeholder="Name"
                                            onChange={(this.handleChange)}
                                            required />
                                    </Form.Group>
                                    <Form.Group controlId="email">
                                        <Form.Label>Enter email</Form.Label>
                                        <Form.Control
                                            type="email"
                                            name="email"
                                            placeholder="Email"
                                            onChange={(this.handleChange)}
                                            required />
                                    </Form.Group>
                                    <Form.Group>
                                        <Button varient="primary" type="submit">
                                            Book Ticket
                                  </Button>
                                    </Form.Group>
                                </Form>
                            </Col>
                        </Row>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button varient="warning" onClick={this.props.onHide}>Close</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}
export default Booking;