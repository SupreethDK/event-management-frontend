import React from "react";
import "../App.css";
import FutureEvent from './FutureEvent';

class Input extends React.Component {

    constructor(props) {
        super();
        this.state = {
            eventName: '',
            eventOrganiser: '',
            eventOrganiserEmail: '',
            eventDate: '',
            eventCost: '',
            location: '',
        }
        this.handleChange = this.handleChange.bind(this);
        this.onSubmitSend = this.onSubmitSend.bind(this);
    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    onSubmitSend(e) {

        e.preventDefault();

        try {
            fetch("https://supreeth-evm.herokuapp.com/events", {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({
                    event_name: this.state.eventName,
                    event_organiser: this.state.eventOrganiser,
                    event_organiser_email: this.state.eventOrganiserEmail,
                    event_date: this.state.eventDate,
                    event_cost: this.state.eventCost,
                    location: this.state.location
                })
            })
            window.location.reload();
        } catch (error) {
            console.error(error);
        }
    }

    render() {
        return (
            <div>
                <p className="h4 text-center p-3 bg-title-color text-white ">Create Event</p>
                <form className="d-flex mt-5  d-flex flex-column justify-content-between align-items-center" onSubmit={this.onSubmitSend}>
                    <label className="text-center mt-2 text-white">
                        Title:
                    <input type="text" name="eventName" className="form-control" onChange={(this.handleChange)} required></input>
                    </label>
                    <label className="text-center mt-3 text-white">
                        Organization:
                    <input type="text" name="eventOrganiser" className="form-control" onChange={(this.handleChange)} required></input>
                    </label>
                    <label className="text-center mt-3 text-white">
                        Email:
                        <input type="text" name="eventOrganiserEmail" className="form-control" onChange={(this.handleChange)} required></input>
                    </label>
                    <label className="text-center mt-3 text-white">
                        Date:
                    <input type="text" name="eventDate" className="form-control" placeholder="yyyy-mm-dd" onChange={(this.handleChange)} required></input>
                    </label>
                    <label className="text-center mt-3 text-white">
                        Price:
                        <label></label>
                        <input type="text" name="eventCost" className="form-control" placeholder="0000.00" onChange={(this.handleChange)} required></input>
                    </label>
                    <label className="text-center mt-3 text-white">
                        Place:
                    <input type="text" name="location" className="form-control" onChange={(this.handleChange)} required></input>
                    </label>
                    <button className="btn btn-success m-5">Add</button>
                </form>
                <div>
                    <FutureEvent />
                </div>
            </div>
        )
    }
}

export default Input;