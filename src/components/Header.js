import React from "react";
import { Link } from "react-router-dom";

function Header() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light d-flex justify-content-between head">
      <div>
        <h1 className='navbar-brand'>Event Management</h1>
      </div>
      <div>
        <ul className="navbar-nav mr-auto ">
          <li className='nav-item'><Link className='nav-link' style={{ textDecoration: 'none' }} to='/'>Create Event</Link></li>
          <li className='nav-item'><Link className='nav-link' style={{ textDecoration: 'none' }} to='/events/past'>Past Events</Link></li>
        </ul>
      </div>
    </nav>
  );
}

export default Header;