import React from "react";
import "../App.css";
class UpdateEvent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            eventDate: '',
            eventCost: '',
            location: '',
            eventId: this.props.match.params.id
        }
        this.handleChange = this.handleChange.bind(this)
        this.onSubmitSend = this.onSubmitSend.bind(this)
    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    onSubmitSend(e) {
        e.preventDefault();

        try {
            let id = this.state.eventId.split(' ');
            let idNumber = id[id.length - 1];
            fetch(`https://supreeth-evm.herokuapp.com/events/update/${idNumber}`, {
                method: "PUT",
                headers: { "Content-Type": "application/json; charset=UTF-8" },
                body: JSON.stringify({
                    event_date: this.state.eventDate,
                    event_cost: this.state.eventCost,
                    location: this.state.location
                })
            })
                .then(res => console.log(res))
        } catch (err) {
            console.error(err.message);
        }
    }

    render() {
        return (
            <div>

                <p className="h4 text-center p-3 mb-2 bg-title-color text-white">Update Event</p>
                <form className="d-flex flex-column justify-content-between align-items-center" onSubmit={this.onSubmitSend}>

                    <label className="text-center mt-3 text-white">
                        Change event date:
                     <input type="text" name="eventDate" className="form-control" placeholder="yyyy-mm-dd" onChange={(this.handleChange)} required></input>
                    </label>

                    <label className="text-center mt-3 text-white">
                        Change event cost:
                         <label></label>
                        <input type="text" name="eventCost" className="form-control" onChange={(this.handleChange)} required></input>
                    </label>

                    <label className="text-center mt-3 text-white">
                        Change event location:
                     <input type="text" name="location" className="form-control" onChange={(this.handleChange)}></input>
                    </label>

                    <button className="btn btn-sm center-block btn-success m-5">Update</button>
                </form>
            </div>
        );
    }
}

export default UpdateEvent;