import React from "react";
import {Link} from "react-router-dom";

class CardFuture extends React.Component {

    render() {
        return (
            <div >
                <div className="row m-5" key={this.props.event.event_id}>
                    <div className="col-sm-12">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">{this.props.event.event_name}</h5>
                                <p className="card-text">{this.props.event.event_date}</p>
                                <p className="card-text">{this.props.event.location}</p>
                                <button className="btn btn-danger btn-sm m-2" id ={this.props.event.event_id} onClick={this.props.onDelete}>Delete</button>
                                <Link className="btn btn-warning btn-sm m-2" to={`/events/update/${this.props.event.event_id}`}  id ={this.props.event.event_id}>Update</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}



export default CardFuture;