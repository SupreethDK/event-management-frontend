import React from 'react';
import './App.css';
import {
  Switch,
  Route,
} from "react-router-dom";
import Header from './components/Header';
import Input from './components/Input';
import PastEvent from './components/PastEvent';
import FutureEvent from './components/FutureEvent';
import UpdateEvent from './components/UpdateEvent';
import Footer from './components/Footer';
import { Provider } from 'react-redux';
import store from './store/store'

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <React.Fragment>
          <Header />
          <div className="bkround">
            <Switch>
              <Route exact path='/' component={Input}></Route>
              <Route path='/events/past' component={PastEvent}></Route>
              <Route path='/events/future' component={FutureEvent}></Route>
              <Route path='/events/update/:id' component={UpdateEvent}></Route>
            </Switch>
          </div>
          <Footer />
        </React.Fragment>
      </Provider>

    );
  }
}

export default App;
