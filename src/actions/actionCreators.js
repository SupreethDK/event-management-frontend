import {  GET_PAST_EVENTS , GET_FUTURE_EVENTS } from './actionTypes';

export const getPastEvents = (res) => {
    return  {
        type : GET_PAST_EVENTS,
        payload : res
    }
}

export const getFutureEvents = (res) => {
    return  {
        type : GET_FUTURE_EVENTS,
        payload : res
    }
}
